import React from 'react';
import './button.css';

const Button = ({ label, clicked, classes }) => {
  return (
    <React.Fragment>
      <button
        data-testid={'button'}
        data-labeltext={'button'}
        className={`app-button ${classes || ''}`}
        onClick={clicked}
        >
          {label}
      </button>
    </React.Fragment>
  );
};

export default Button;