import React from 'react';
import './chip.css';

const Chip = ({ message, classes, type }) => {

  const selectedType = ['primary', 'success', 'warning', 'danger'].find(t => t === (type ? type.toLowerCase() : 'primary'));

  return (
    <React.Fragment>
      <div
        data-test-id={'chip'}
        className={`app-chip ${selectedType || 'primary'} ${classes || ''}`}
      >
        {message}
      </div>
    </React.Fragment>
  );
};

export default Chip;