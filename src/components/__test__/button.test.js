import React from 'react';
import ReactDOM from 'react-dom';
import { Button, Chip } from './../index';

import {cleanup, fireEvent, render} from '@testing-library/react';

it('Button renders without crashing...', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Button />, div);
});

it('Chip renders without crashing...', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Chip />, div);
});

it('renders button correctly', () => {
  const { queryByLabelText, getByLabelText, getByTestId } = render(
    <Button label="Primary" classes={'m-l-10 m-b-10 primary'} />
  );

  expect(getByTestId('button')).toHaveTextContent(`Primary`);
});