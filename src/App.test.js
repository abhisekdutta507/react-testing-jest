import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('App renders Primary button', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Primary/i);
  expect(linkElement).toBeInTheDocument();
});
