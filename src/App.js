import React, { useState, useEffect } from 'react';
import './App.css';

import { Button, Chip } from './components';

const App = () => {
  const [state, setState] = useState({
    message: '',
    chipType: '',
    showChip: false,
    chipCloseDelay: 0,
    chipCounter: 0,
    chipCloseTimeout: 0
  });

  const Primary = () => {
    setState({ ...state, chipCounter: state.chipCounter + 1, showChip: true, message: 'It\'s a click.', chipType: 'Primary', chipCloseDelay: 3000 });
  };

  const Success = () => {
    setState({ ...state, chipCounter: state.chipCounter + 1, showChip: true, message: 'You clicked it.', chipType: 'success', chipCloseDelay: 3000 });
  };
  
  const Warning = () => {
    setState({ ...state, chipCounter: state.chipCounter + 1, showChip: true, message: 'A click is detected!', chipType: 'warning', chipCloseDelay: 3000 });
  };

  const Danger = () => {
    setState({ ...state, chipCounter: state.chipCounter + 1, showChip: true, message: 'Click can be dangerous', chipType: 'Danger', chipCloseDelay: 3000 });
  };

  const ChipClose = () => {
    setState({ ...state, showChip: false, message: '' });
  };

  const ChipCounted = () => {
    const { chipCloseTimeout, chipCloseDelay } = state;
    
    clearTimeout(chipCloseTimeout);
    setState({ ...state, chipCloseTimeout: setTimeout(ChipClose, chipCloseDelay) });
  };

  useEffect(ChipCounted, [state.chipCounter]);

  const { showChip, message, chipType } = state;

  return (
    <div className="App p-v-10">
      <Button label="Primary" clicked={Primary} classes={'m-l-10 m-b-10 primary'} />
      <Button label="Success" clicked={Success} classes={'m-l-10 m-b-10 success'} />
      <Button label="Warning" clicked={Warning} classes={'m-l-10 m-b-10 warning'} />
      <Button label="Danger" clicked={Danger} classes={'m-l-10 m-b-10 danger'} />

      {
        showChip && <Chip message={message} type={chipType} />
      }
    </div>
  );
};

export default App;
